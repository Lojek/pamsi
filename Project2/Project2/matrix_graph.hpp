#pragma once

#include "graph.hpp" 

#include <cstdlib>
#include <memory>


class MatrixGraph : public Graph {

	std::unique_ptr<std::unique_ptr<int[]>[]> matrix; 
														
public:
	
	void fillGraph(const bool allowLoops) const;
	void printGraph() const;
	const int readFromFile();

	const int& getWeight(int t_row, int t_column) const { return matrix[t_row][t_column]; } 

	MatrixGraph(int t_V, double t_density);
	MatrixGraph() : Graph() {};
};