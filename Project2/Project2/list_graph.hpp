#pragma once

#include "graph.hpp"


struct Edge
{
	int source, dest, weight; 
};



class ListGraph : public Graph 
{
	Edge* edge; //array of all edges in the graph


public:
	
	void fillGraph(const bool allowLoops) const; 
	void printGraph() const;
	const int readFromFile();
	const Edge* getStruct() const { return edge; } 
	void addEdge(int t_guardNumber, int t_number, int t_weight, int flag) const; 
	bool detectIfExist(int t_guardNumber, int t_nodeNumber) const; 
	
	explicit ListGraph(int t_V, double t_density)
		:Graph(t_V, static_cast<int>(t_density* t_V* (t_V - 1)), t_density),
		 edge(new Edge[static_cast<int>(t_density* t_V* (t_V - 1))]) {}
	ListGraph() : Graph() {};
	~ListGraph() { delete[] edge; }
};
