#include <iostream>
#include <cstdlib>
void quickSort(int *tab, int start, int end);
int partition(int *tab, int start, int end);
void quickWrap(int *tab, int start, int end, int maxdepth);