#include <iostream>
#include <cstdlib>
void merge(int tab[], int start, int mid, int end);
void mergeSort(int tab[], int start, int end);
void mergeWrap(int *tab, int start, int end, int maxdepth);