#include "InsertionSort.h"



void insertionSort(int *tab, int start, int end)
{
	for (int i = start + 1; i <= end; i++)
	{
		int key = tab[i];
		int j = i;

		while (j > start && tab[j - 1] > key)
		{
			tab[j] = tab[j - 1];
			j--;
		}
		tab[j] = key;
	}
}