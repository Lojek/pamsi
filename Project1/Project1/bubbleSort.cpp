#include <iostream>
#include <cstdlib>
#include <time.h>
#include "BubbleSort.h"

void bubblesort(int tab[], int s)
{
	int variable;
bool swapped = 0;
	do {
		swapped = 0;
		for (int i = 0; i < s - 1; i++)
		{

			if (tab[i] > tab[i + 1])
			{
				variable = tab[i + 1];
				tab[i + 1] = tab[i];
				tab[i] = variable;
				swapped = 1;
			}
		}
		
	}while (swapped);

}
	
