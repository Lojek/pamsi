#include "QuickSort.h"
int partition(int *tab, int start, int end)
{
	int pivot = tab[(start + end) / 2], i = start, j = end;

	while (true)
	{
		while (tab[j] > pivot) j--;

		while (tab[i] < pivot) i++;

		if (i < j)
			std::swap(tab[i++], tab[j--]);

		else
			return j;
	}
}


void quickSort(int *tab, int start, int end)
{
	if (start < end)
	{
		int q = partition(tab, start, end);
		quickSort(tab, start, q);
		quickSort(tab, q + 1, end);
	}
}

void quickWrap(int *tab, int start, int end, int maxdepth)
{
	quickSort(tab, start, end);
}