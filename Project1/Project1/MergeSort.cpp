#include "MergeSort.h"




void merge(int tab[], int start, int mid, int end)
{
	int i, j, k;
	int s1 = mid - start + 1;
	int s2 = end - mid;

	int *tab1 = new int[s1];
	int *tab2 = new int[s2];

	for (i = 0; i < s1; i++)
		tab1[i] = tab[start + i];
	for (j = 0; j < s2; j++)
		tab2[j] = tab[mid + 1 + j];

	
	i = 0; 
	j = 0; 
	k = start;
	while (i < s1 && j < s2)
	{
		if (tab1[i] <= tab2[j])
		{
			tab[k] = tab1[i];
			i++;
		}
		else
		{
			tab[k] = tab2[j];
			j++;
		}
		k++;
	}

	while (i < s1)
	{
		tab[k] = tab1[i];
		i++;
		k++;
	}

	while (j < s2)
	{
		tab[k] = tab2[j];
		j++;
		k++;
	}
	delete[] tab1;
	delete[] tab2;
}


void mergeSort(int tab[], int start, int end)
{
	if (start < end)
	{
	
		
		int mid = start + (end - start) / 2;

		mergeSort(tab, start, mid);
		mergeSort(tab, mid + 1, end);

		merge(tab, start, mid, end);
	}
}

void mergeWrap(int *tab, int start, int end, int maxdepth)
{
	mergeSort(tab, start, end);
}
