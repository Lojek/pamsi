#include "IntroSort.h"



void heapSort(int *left, int *right)
{
	std::make_heap(left, right);
	std::sort_heap(left, right);
}


void introSort(int *tab, int *left, int *right, int maxdepth)
{
	if ((right - left) < 16)
		insertionSort(tab, left - tab, right - tab);

	else if (maxdepth == 0)
		heapSort(left, right + 1);

	else
	{
		int pivot = partition(tab, left - tab, right - tab);
		introSort(tab, left, tab + pivot, maxdepth - 1);
		introSort(tab, tab + pivot + 1, right, maxdepth - 1);
	}
}


void introWrap(int *tab, int start, int end, int maxdepth)
{
	introSort(tab, tab, tab + end, maxdepth);
}