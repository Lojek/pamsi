#include<iostream>
#include<stdio.h> 
#include<algorithm> 
#include <string>
#include <ctime>
#include <vector>

#define COMPUTER 1 
#define HUMAN 2 

// Computer will move with 'O' 
// and human with 'X' 
#define COMPUTERMOVE 'O' 
#define HUMANMOVE 'X' 

int totalMoves = 0,move;


void showBoard(char board[])
{
	std::cout << std::endl;
	std::cout << board[0] << " | " << board[1] << " | " << board[2] << std::endl;
	std::cout << "--+---+--" << std::endl;
	std::cout << board[3] << " | " << board[4] << " | " << board[5] << std::endl;
	std::cout << "--+---+--" << std::endl;
	std::cout << board[6] << " | " << board[7] << " | " << board[8] << std::endl;
	std::cout << std::endl;
}
void emptyBoard(char board[])
{
	for(int i=0; i<9; i++)
	{
		board[i] = ' ';
	}
}
void makeAMove(char znak, char board[], int field)
{
	board[field] = znak;
	totalMoves++;
	return;
}
char checkWhoWin(char board[])
{
	int z = 0;
	
	for (int i = 0; i < 9; i + 3)
	{
		for (int j = 1; j < 3; j++)
		{
			if (board[i] == board[j + i])
				z++;
		}
		if (z == 2)
		{
			return board[i];
		}

		else z = 0;
	}
	for (int j = 0; j < 3; j++)
	{
		for (int i = 0; i < 9; i + 3)
		{
			if (board[j] == board[i+j])
				z++;
			if (z == 2)
			{
				return board[j];
			}
			else z = 0;

		}

		if (board[0] == board[4] && board[0] == board[8])
		{
			return board[0];
		}
		if (board[2] == board[4] && board[2] == board[6])
		{
			return board[2];
		}
		
	}
	return 'N';
}
void currentValidMoves(char board[], std::vector < int > validMoves)
{
	for(int i=0;i<9;i++)
	{
		if (board[i] == ' ')
			validMoves.push_back(i);
	}
	return;
}

int evaluateMove(int move, char board[])
{
	char board2[9];
	for (int i = 0; i < 9; i++)
	{
		board2[i] = board[i];
	}
	makeAMove('O', board2, move);
	if (checkWhoWin(board2) == 'X')
		return -10;
	if (checkWhoWin(board2) == 'O')
		return 10;
	if (checkWhoWin(board2) == 'N')
		return 0;

}
bool moveIsValid(int m, char board[])
{
	if (board[m] != 'X' && board[m] != 'O')
	{
		return true;
	}

	else
	{
		std::cout << "this move is invalid!!! Try again\n";
		return false;
	}
}
int bestMove(char board[], std::vector < int > validMoves)
{
	int bestMoveIs = NULL;
	for (int i = 0; i < validMoves.size(); i++)
	{
		move = validMoves[i];
		if (evaluateMove(move,board) >= evaluateMove(bestMoveIs,board))
			bestMoveIs = move;
	}
	return bestMoveIs;
}
void playerMakeAMove(char board[]) 
{
	int field;
	showBoard(board);
	std::cout << "Give me the number of field you want to fill with your mark\n";
	std::cin >> field;
	if(moveIsValid(field,board))
	{
		makeAMove('x',board,field);
	}
	showBoard(board);
	return;
}
void computerMakeAMove(char board[])
{
	std::vector < int > validMoves;
	currentValidMoves(board, validMoves);
	makeAMove('O',board, bestMove(board,  validMoves));
	std::cout << "computer just made his move\n";
	
	return;
}
void play(char board[])
{
	
	computerMakeAMove(board);
	playerMakeAMove(board);
	if (checkWhoWin(board) != 'N' || totalMoves != 9)
		return;
	play(board);

}
void playTheGame(char board[]) 
{
	char answer;
	std::cout << "who should start? Type p for player or c for computer\n";
	std::cin >> answer;
	switch (answer) {
	case ('p'):
	
		playerMakeAMove(board);
		
	case ('c'):
		computerMakeAMove(board);
		
	}
	play(board);
}

